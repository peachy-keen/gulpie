#!/usr/bin/env node

require('babel/register');
require('./../src/task-manager')

  .load()

  .then(function() {
    process.chdir(__dirname+'/../src')
    require('gulp-cli')();
  })

  .catch(function (err) {
    console.error(err);
  });
