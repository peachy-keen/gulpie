## Gulpie


Gulpie is collection of common gulp tasks wrapped into a nice easy to configure tool. This comes with preconfigured tasks, but there is not much to the wrapping that gulpie provides. That probably won't change much. You can just fork this and use whatever tasks you want.

**Philosophy**

Most environments are not so varied that you can't use the same tasks over and over. Gulp by itself offers a set of knives, but what we need is a pocket knife that we can carry in our pocket wherever we go. Sure, you're trading convenience for flexibility, but once you've got everything figured out you don't need the flexibility anymore. Even if you're never 100% settled on your tasks, you at least now have a core tooling to build from.

### Installation

For npm > 1.1.65, you can just specify the github details:

```json
"dependencies": {
    "gulpie": "castiron/gulpie"
},
```

### Basic Usage

You use gulpie the same as you would gulp:

```
gulpie <task> <options>
```

The difference is that you don't have a `gulpfile.js` file in your project, nor any gulp tasks anywhere (though you could). That's already done for you with gulpie.

### Manifest File

Gulpie uses a manifest file named `gulpie.json` to load any configurations. 

**Task Configurations**

There are several task-based configurations that can be applied globally (i.e. for scripts, styles, tests, etc.). These are rather straight-forward, however you'll need to know what options are available. And those are listed in [/src/tasks/README.md](/src/tasks/README.md).

**Setups**

However, beyond configuring tasks, we also need to configure the necessary directories and environments. For this, there are "setups". 

You can have as many setups as you want and depending on a few factors multiple setups can be applied at the same time. For example, you may have a setup that produces assets for your website but a totally different setup for producing assets for your blog in the same project. You'd want both of these setups to be applied when the tasks are run. The only required value i `in`, but you'll likely want to override the defaults. 

  

An example manifest:

```json
{
	"tasks": {
		"scripts": {
			"lint": ["jshint"],
			"smush": ["concat", "uglify"],
			"preprocess": ["babel"],
			"optimize": [],
			"loader": "system"
		},
		
		"styles": {
			"lint": [],
			"smush": ["concat"],
			"preprocess": ["sass"],
			"optimize": []
		},
		
		"tests": {
			"preprocess": ["babel"],
			"runner": "jasmine",
			"karma": false
		}, 
		
		"html": {

		}
	},

	"setups": {
		"site": {
			"in": ["/app/src/"],
			"out": "/dist",
		}
	}
}
```