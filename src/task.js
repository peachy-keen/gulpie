// a base task that others will extend from


export default class Task {

  // the "setup" comes from the gulpie.json file
  // it can contain any special configurations
  constructor(setup) {
    this.setup = setup;
  }

  // return the command that triggers (and groups) this task
  static command() {
    throw "task must have a command";
  }

  // return a list of other task keys that should run before this one
  static deps() {
    return [];
  }

  // actually run the task
  run() {
    return true;
  }

  // asynchronous setup can happen here
  init() {
    return new Promise((yes, no) => yes());
  }

  // list the files produced by this
  // task that can be "cleaned"
  cleanable() {
    return new Promise((yes, no) => yes());
  }

  static clientRoot() {
    return new Promise((yes, no) => {
      exec('pwd', {encoding: 'utf8'}, (e, pwd) => {
        if (e) no(e);
        yes(pwd.replace(/\n$/, ''));
      });
    });
  }
}
