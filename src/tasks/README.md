## Tasks

There are only these tasks:

* `lint` - static analysis
* `build` - will run `lint` first
* `watch` - reruns `build` on change
* `clean` - delete files produced by `build`
* `serve` - serves files but also runs `watch`

You can run these tasks with one of these three environments:

* `dev` - _default_
* `prod` 
* `test`

For example:

```
gulpie --env=dev watch
```


### Implementation Details

There are the "main" tasks (`lint`, `build`, `watch`, ...), but there are sub-tasks too. Unlike the sub-tasks, there is no gulp "task" that can be run from the command line. You can't somehow only build all the coffeescript. All you get is `gulpie build`, which would do all the coffeescript plus all the sass plus all the linting and everything else. 

Each task works on one "setup" at a time. It's the TaskManager's job to re-run the task for each setup. So if there are three setups, running `gulpie build` on the command would run the Build task on each setup. But as far as gulp is concerned, it only ran the task once. 

Tasks don't have to do anything. If linting only needs to happen while in the "dev" environment, then so be it. 

Tasks must all report what files they produce that can be removed by the `clean` task. 

How are we going to handle dependencies???? Particularly build dependencies? Lint before or after compiling?