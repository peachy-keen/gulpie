// the task manager coordinates whether or not tasks
// can run and should be registered with gulp

import gulp from 'gulp';
import Dir from './tools/dir';
import path from 'path';
import {log} from 'gulp-util';
var PathUtility = path;
import {exec} from 'child_process';
import {objectToMap, collectPromises} from './tools/funcs';
import Setup from './setup';


class TaskManager {

  // called first by our runner script
  // load and prep tasks, so we can later define them
  load() {
    this.tasks = new Map();
    this.cmds = new Map();
    return new Promise((ready, ornot) => {
      Dir.load('/gulpie.json')
        .then(this.buildSetups.bind(this), ornot)
        .then(this.loadTasks.bind(this), ornot)
        .then(collectPromises(this.buildTasks.bind(this)))
        .then(ready, ornot);
    });
  }

  // called from the gulpfile, but after we've
  // already prepped all the tasks
  define(tasks) {

    // create the tasks
    this.tasks.forEach((taskInstance, meta) => {
      gulp.task(meta.name, meta.deps, taskInstance.run.bind(taskInstance));
    });

    // create the commands with the tasks as dependencies
    this.cmds.forEach((tasks, cmd) => {
      gulp.task(cmd, tasks);
    });
  }

  // get a map of setups as defined by the gulpie.json manifest
  buildSetups(manifest) {
    return new Promise((yes, no) => {
      if (!manifest.setups) no("No setups defined");
      const defaults = manifest.defaults ? manifest.defaults : {};
      this.setups = objectToMap(manifest.setups, (v, k) => {
        return new Setup(k, Object.assign(defaults, v));
      });

      yes();
    });
  }

  // load the tasks from the tasks directory
  loadTasks() {
    process.chdir(__dirname+'/..');
    return Dir.load('/src/tasks/*.js');
  }

  // instantiate task(s) and initialize them
  buildTasks(TaskClass, taskFileName) {
    return collectPromises((setupConf, setupKey) => {
      return this.buildIndividualTask(TaskClass, taskFileName, setupConf, setupKey);
    })(this.setups);
  }

  // creates a new task instance (per setup)
  buildIndividualTask(TaskClass, taskFileName, setupConf, setupKey) {
    var taskInstance = new TaskClass(setupConf),
      cmd = TaskClass.command(),
      taskName = PathUtility.basename(taskFileName, '.js'),
      fullName = `${cmd}:${setupKey}:${taskName}`, // i.e. compile:blog:sass
      metaObj = {
        setup: setupKey,
        cmd: cmd,
        task: taskName,
        name: fullName,
        deps: TaskClass.deps()
      };

    this.tasks.set(metaObj, taskInstance);

    if (!this.cmds.has(cmd)) {
      this.cmds.set(cmd, []);
    }
    this.cmds.get(cmd).push(fullName);

    return taskInstance.init();
  }


  // catchall
  error(err) {
    if (!err) err = "Oops! Something went wrong!";
    console.error(err);
    throw err;
  }

}

export default new TaskManager();
