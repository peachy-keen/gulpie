// virtual representation of a file system directory

import glob from 'glob';
import path from 'path';
import fs from 'fs';

var Path = path;

var readFile = (filename, encoding = 'utf8') => {
  return new Promise((yes, no) => {
    fs.readFile(filename, {encoding: encoding}, (err, data) => {
      err ? no(err) : yes(data);
    });
  });
};

export default class Dir {

  // represent a directory
  constructor() {
    this.cwd = Dir.root;
    this.atRoot = true;
  }

  // shortct to make a Dir for a path
  static from(path) {
    return new Dir().cd(path);
  }

  // need these to use glob correctly
  get globOptions() {
    var cwd = this.atRoot ? Dir.root : path.join(Dir.root, this.cwd);
    return {
      root: cwd,
      cwd: cwd,
    }
  }

  // use gulpie root unless specified otherwise
  static get root() {
    var r = process.cwd();
    return r ? r : __dirname+'/../..';
  }

  // changes the directory that globs are based on
  cd(path) {
    this.cwd = path;
    this.atRoot = false;
    return this;
  }

  // loads the contents of *first matching* file as a string
  read(path) {
    return new Promise((yes, no) => {
      this.exists(path)
        .then((matches) => {
          return readFile(matches[0]);
        }, no).then((data) => { yes(data); });
    })
  }


  // requires javascript/json files
  // path is relative to root (unlike normal 'require' calls)
  // for multiple files, returns a Map (basename => content)
  load(path) {
    return new Promise((yes, no) => {
      this.exists(path)
        .then((matches) => {
          try {
            if (matches.length > 1) {
              let res = new Map();
              matches.forEach((m) => {
                res.set(Path.basename(m), require(m));
              });
              yes(res);
            } else {
              yes(require(matches[0]));
            }
          } catch (err) {
            no(err);
          }
        }, (err) => {
          no(err);
        });
    });
  }


  // lists files in this directory
  ls(ext) {
    // todo
  }

  // checks if files exist according to the given glob
  exists(glb) {
    return new Promise((yes, no) => {
      this._glob(glb, (err, matches) => {
        if (err) return no(err);
        if (matches.length == 0) no("Found no matches: "+glb);
        return yes(matches);
      });
    });
  }

  // static wrappers

  static read(path) {
    return new Dir().read(path);
  }
  static load(path) {
    return new Dir().load(path);
  }
  static exists(glb) {
    return new Dir().exists(glb);
  }

  // private

  _glob(pattern, opts, cb) {
    var [p, ...tail] = arguments;
    var c = tail.pop();
    var o = tail.pop();
    o = Object.assign({}, this.globOptions, o);
    glob(p, o, c);
  }
}
