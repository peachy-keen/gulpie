
export function objectToMap(obj = null, cb = null) {
  let m = new Map();
  for(let key of Object.keys(obj)) {
    let val = cb ? cb(obj[key], key) : obj[key];
    m.set(key, val);
  }
  return m;
};


export function collectPromises(cb) {
  return (map) => {
    var promises = [];

    for(let entry of map) {
      promises.push(cb(entry[1], entry[0]));
    }
    return Promise.all(promises);
  };
}
