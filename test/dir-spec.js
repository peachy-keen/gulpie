import Dir from '../src/tools/dir';


process.chdir(__dirname+'/sample');

// a "done" wrapper that ignores args
var undone = (done) => { return () => done()},

  // throws an error if called, may throw the given arg
  thrower = (err) => { throw err;},

  // accept any promise if fulfilled
  acceptAnyGoodPromise = (p, done) => {
    p.then(undone(done)).catch(thrower);
  },

  // accept any promise if rejected
  acceptAnyBadPromise = (p, done) => {
    p.catch(undone(done)).then(() => { throw 'expected error'; });
  };


describe('Dir', () => {

  it('checks that files exists', (done) => {
    acceptAnyGoodPromise(
      Dir.exists('/files/plain.txt'), done);
  });

  it('checks that multiple files exist', (done) => {
    acceptAnyGoodPromise(
      Dir.exists('/files/*.json'), done);
  });

  it("knows that files don't exist", (done) => {
    acceptAnyBadPromise(
      Dir.exists('/files/____plain.txt'), done);
  });

  it("confirms that files exist in it's directory", (done) => {
    acceptAnyGoodPromise(
      Dir.from('/files').exists('plain.txt'), done);
  });

  it('works with relative directories, from root', (done) => {
    acceptAnyGoodPromise(
      Dir.exists('files/alpha/../plain.txt'), done);
  });

  it('works with relative directories, not from root', (done) => {
    acceptAnyGoodPromise(
      Dir.from('/files/alpha').exists('../plain.txt'), done);
  });

  it('when not at root, treats paths as relative', (done) => {
    acceptAnyGoodPromise(
      Dir.from('/files').exists('/plain.txt'), done);
  });

  it('reads a file', (done) => {
    Dir.read('/files/plain.txt')
      .then(res => {
        if (res == "this is a file. don't change it.\n") {
          done();
        } else {
          throw "doesn't match";
        }
      }, thrower);
  });

  it('loads a file', (done) => {
    acceptAnyGoodPromise(Dir.load('/files/plain.json'), done);
  });

  it("errs when loading nonexistent file", (done) => {
    acceptAnyBadPromise(Dir.load('/files/_____plain.json'), done);
  });

  it('loads a file with correct contents', (done) => {
    Dir.load('/files/plain.json').then((obj) => {
      if (obj.fruits && obj.fruits[0] == 'apple') {
        done();
      } else {
        throw "did not get the right json";
      }
    }, thrower);
  });

  it('loads a relative file with correct contents', (done) => {
    Dir.from('/files').load('/plain.json').then((obj) => {
      if (obj.fruits && obj.fruits[0] == 'apple') {
        done();
      } else {
        throw "did not get the right json";
      }
    }, thrower);
  });

  it('loads multiple files and returns an array', (done) => {
    Dir.load('/files/*.json').then((arr) => {
      if (arr.size == 2) {
        var obj = arr.get('plain2.json');
        if (obj.element == 'water') {
          done();
        }
        throw "Got objects, but wrong content";
      }
      throw "Incorrect amount of files loaded";
    }, thrower);
  });

});
