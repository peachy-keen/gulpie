// This file exists as a proxy to test how modules behave
// when imported by a different project (the 'sample' project).

export var Manifest = require('gulpie/src/manifest');
export var Dir = require('gulpie/src/tools/dir');
